package ru.khusnullin.aircraftpositions.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.khusnullin.aircraftpositions.repository.AircraftRepository;

@RequiredArgsConstructor
@Controller
public class PositionController {
	@NonNull
	private final AircraftRepository repository;

	@GetMapping("/aircraft")
	public String getCurrentAircraftPositions(Model model) {
		model.addAttribute("currentPositions", repository.findAll());
		return "positions";
	}

}
