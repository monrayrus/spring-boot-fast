package ru.khusnullin.aircraftpositions.repository;

import org.springframework.data.repository.CrudRepository;

import ru.khusnullin.aircraftpositions.models.Aircraft;

public interface AircraftRepository extends CrudRepository<Aircraft, Long> {

}
