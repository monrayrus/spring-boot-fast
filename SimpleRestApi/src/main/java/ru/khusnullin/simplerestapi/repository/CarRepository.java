package ru.khusnullin.simplerestapi.repository;

import org.springframework.data.repository.CrudRepository;

import ru.khusnullin.simplerestapi.models.Car;

public interface CarRepository extends CrudRepository<Car, String>{

}
