package ru.khusnullin.simplerestapi.repository;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import ru.khusnullin.simplerestapi.models.Car;

@Component
public class DataLoader {

	private final CarRepository carRepository;
	
	public DataLoader(CarRepository carRepository) {
		this.carRepository = carRepository;
	}
	
	@PostConstruct
	private void loadData() {
		carRepository.saveAll(List.of(
				new Car("Lada"),
				new Car("Nissan"),
				new Car("Volvo"),
				new Car("Ford")));
	}
}
