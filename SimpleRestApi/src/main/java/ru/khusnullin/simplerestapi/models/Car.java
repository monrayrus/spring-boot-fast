package ru.khusnullin.simplerestapi.models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Car {
	
	@Id
	private String id;
	
	private String model;
	
	public Car() {}
	
	public Car(String id, String model) {
		this.id = id;
		this.model = model;
	}
	
	public Car(String model) {
		this(UUID.randomUUID().toString(), model);
	}
	
	public String getId() {
		return id;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public void setId(String id) {
		this.id = id;
	}
 }
