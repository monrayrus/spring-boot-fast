package ru.khusnullin.simplerestapi.controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.khusnullin.simplerestapi.models.Car;
import ru.khusnullin.simplerestapi.repository.CarRepository;

@RestController
@RequestMapping("/cars")
public class CarController {
	private final CarRepository carRepository;
	
	public CarController(CarRepository carRepository) {
		this.carRepository = carRepository;
	}
	
	@GetMapping
	Iterable<Car> getCars() {
		return carRepository.findAll();
	}
	
	@GetMapping("/{id}")
	Optional<Car> getCarById(@PathVariable String id) {
		return carRepository.findById(id);
	}
	
	@PostMapping
	Car addCar(@RequestBody Car car) {
		return carRepository.save(car);
	}
	
	@PutMapping("/{id}")
	ResponseEntity<Car> putCar(@PathVariable String id, @RequestBody Car car) {

		return (carRepository.existsById(id)) ? new ResponseEntity<>(carRepository.save(car), HttpStatus.OK) :
			 									new ResponseEntity<>(carRepository.save(car), HttpStatus.CREATED);

	}
	
	@DeleteMapping("/{id}")
	void deleteCar(@PathVariable String id) {
		carRepository.deleteById(id);
	}
}
