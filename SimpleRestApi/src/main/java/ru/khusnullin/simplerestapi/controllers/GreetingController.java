package ru.khusnullin.simplerestapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.khusnullin.simplerestapi.models.Greeting;

@RestController
@RequestMapping("/greeting")
public class GreetingController {
	
	private final Greeting greeting;	
	
	public GreetingController(Greeting greeting) {
		this.greeting = greeting;
	}
	
	@GetMapping
	String getGreeting() {
		return greeting.getName();
	}
	
	@GetMapping("/car")
	String getNameAndCar() {
		return greeting.getCar();
	}
}
