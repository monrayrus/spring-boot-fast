package ru.khusnullin.planefinder.service;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import ru.khusnullin.planefinder.models.Aircraft;
import ru.khusnullin.planefinder.repository.AircraftRepository;

@EnableScheduling
@Component
public class PlaneFinderPoller {

	private WebClient client = WebClient.create("http://localhost:7634/aircraft");
	
	private final RedisConnectionFactory factory;
	private final AircraftRepository aircraftRepository;
	
	PlaneFinderPoller(RedisConnectionFactory factory, AircraftRepository aircraftRepository) {
		this.factory = factory;
		this.aircraftRepository = aircraftRepository;
	}
	
	/*
	 * Для реализации опроса по заданному расписанию воспользуемся аннотацией @EnableScheduling,
	 * размещенной ранее на уровне класса, и снабдим созданный метод pollPlanes()
	 * аннотацией @Scheduled, передав в нее параметр fixedDelay=1000, указывающий, что опрос выполняется каждые 1000 мс (один раз в секунду).
	 */
	@Scheduled(fixedRate = 1000) 
	private void pollPlanes() {
		factory.getConnection().serverCommands().flushDb();
		
		client.get()
				.retrieve()
				.bodyToFlux(Aircraft.class)
				.filter(plane -> !plane.getReg.isEmpty())
				.toStream()
				.forEach(aircraftRepository::save);
		
		aircraftRepository.findAll().forEach(System.out::println);
	}
}
