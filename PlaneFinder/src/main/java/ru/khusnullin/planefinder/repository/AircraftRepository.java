package ru.khusnullin.planefinder.repository;

import org.springframework.data.repository.CrudRepository;

import ru.khusnullin.planefinder.models.Aircraft;

public interface AircraftRepository extends CrudRepository<Aircraft, Long> {

}
