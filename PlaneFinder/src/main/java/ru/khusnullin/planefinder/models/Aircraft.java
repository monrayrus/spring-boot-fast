package ru.khusnullin.planefinder.models;

import java.time.Instant;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//указывает механизмам десериализации Jackson игнорировать поля в JSON-ответах, для которых не существует соответствующей переменной экземпляра
@JsonIgnoreProperties(ignoreUnknown = true)
@RedisHash
public class Aircraft {
	@Id
	private Long id;
	private String callsign, squawk, reg, flightno, route, type, category;
	private int altitude, heading, speed;
	// связывает переменную-член класса с полем JSON с другим именем
	@JsonProperty("vert_rate")
	private int vertRate;
	@JsonProperty("selected_altitude")
	private int selectedAltitude;
	private double lat, lon, barometer;
	@JsonProperty("polar_distance")
	private double polarDistance;
	@JsonProperty("polar_bearing")
	private double polarBearing;
	@JsonProperty("is_adsb")
	private boolean isADSB;
	@JsonProperty("is_on_ground")
	private boolean isOnGround;
	@JsonProperty("last_seen_time")
	private Instant lastSeenTime;
	@JsonProperty("pos_update_time")
	private Instant posUpdateTime;
	@JsonProperty("bds40_seen_time")
	private Instant bds40SeenTime;
}
