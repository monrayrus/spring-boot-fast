package ru.khusnullin.springbootcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khusnullin.springbootcrud.models.User;

public interface UsersRepository extends JpaRepository<User, Long> {
}
