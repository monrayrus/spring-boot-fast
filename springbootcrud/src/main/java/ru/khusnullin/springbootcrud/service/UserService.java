package ru.khusnullin.springbootcrud.service;

import ru.khusnullin.springbootcrud.models.User;

import java.util.List;

public interface UserService {
     User findById(Long id);
     List<User> findAll();
     void createUser(User user);
     void updateById(Long id, String firstName, String lastName);
     void deleteById(Long id);
}
