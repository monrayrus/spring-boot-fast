package ru.khusnullin.springbootcrud.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.khusnullin.springbootcrud.models.User;
import ru.khusnullin.springbootcrud.repository.UsersRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UsersRepository repository;

    @Override
    public User findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public void createUser(User user) {
        repository.save(user);
    }

    @Override
    public void updateById(Long id, String firstName,String lastName) {
        User user = repository.getReferenceById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
